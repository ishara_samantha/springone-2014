package com.example.echo;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.rest.api.exceptions.ResourceNotFoundRestApiException;

@RestController
public class EchoController {

	Map<String, EchoRequestJson> responses = new ConcurrentHashMap<>();
	
	@RequestMapping(value = "/echo/add/**", method = RequestMethod.POST)
	public void get(@RequestBody  EchoRequestJson responseJson) {

		String key = responseJson.getMethod() + responseJson.getUrl();
		this.responses.put(key, responseJson);		
	}
	
	@RequestMapping("/echo/**") 
	public ResponseEntity<String> respond (HttpServletRequest request)
	{
		String requestURI = request.getRequestURI();
		String key = request.getMethod() + requestURI;

		EchoRequestJson response = this.responses.get(key);
		if(response == null) {
			throw new ResourceNotFoundRestApiException().userMessage("'%s' is not registered",requestURI);
		} 

		HttpHeaders headers = new HttpHeaders();
		MediaType mediaType = MediaType.valueOf(response.getResponseContentType());
		headers.setContentType(mediaType);
		
		HttpStatus responseCode = HttpStatus.valueOf(response.getResponseCode());
		ResponseEntity<String> result = new ResponseEntity<String>(response.getResponseBody(), headers, responseCode);
		
		
		return result; 	
	}
}
