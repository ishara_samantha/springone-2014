package com.example.spa.site;

import com.example.rest.api.UrlSpace;

//@formatter:off
public class SiteUrls implements UrlSpace 
{
	// main page 
	public static final String ROOT                  = "/";
	
	// forgot password 
	public static final String RESET                 = "/reset";
	public static final String RESET_CODE            = "/reset/{code}";

	// user registration 
	public static final String REGISTER 		  	    = "/register";
	public static final String REGISTER_WELCOME_CODE = "/register/welcome/{code}";
	
	// Manage email subscriptions  
	public static final String EMAIL_SUBSCRIBE       = "/emai/unsubscribe/{token}";
	public static final String EMAIL_UNSUBSCRIBE     = "/emai/unsubscribe/{token}";
	
	// marketing pages 
	public static final String CONTACTS              = "/contacts";
	public static final String PRICING               = "/pricing"; 
	public static final String TERMS                 = "/terms";
	public static final String ABOUT                 = "/about";
	public static final String LOGIN_ERROR           = "/login-error";
	public static final String LOGIN                 = "/login";
}
//@formatter:on
