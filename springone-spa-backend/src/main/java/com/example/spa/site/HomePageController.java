package com.example.spa.site;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.util.SpringSecurityUtils;


@Controller
public class HomePageController {
	
	@RequestMapping(SiteUrls.ROOT)
	public String get(Model model)
	{
		if (SpringSecurityUtils.isAuthenticated())
		{
			return "app";
		}
		else
		{
			return "login";
		}
	}
}
