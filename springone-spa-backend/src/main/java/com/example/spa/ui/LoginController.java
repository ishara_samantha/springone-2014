package com.example.spa.ui;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {

	@RequestMapping("/login")
	public String login()
	{
		return "login"; 
	}
	
	 // Login form with error
	@RequestMapping("/login-error")
	public String loginError(Model model) {
		model.addAttribute("loginError", true);
		return "login";
	}
  
	
	@RequestMapping("/expire")
	@ResponseBody
	public String expire(HttpServletRequest request) {
		
		request.getSession().invalidate();
		return "Session has been expired";
	}
	  
	 
}
