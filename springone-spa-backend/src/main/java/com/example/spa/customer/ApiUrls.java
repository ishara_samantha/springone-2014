package com.example.spa.customer;

import com.example.rest.api.UrlSpace;

public class ApiUrls implements UrlSpace {

	public static final String CUSTOMERS = "/api/customers";
	public static final String CUSTOMER  = "/api/customers/{id}";
}
