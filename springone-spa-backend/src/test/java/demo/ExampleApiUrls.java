package demo;

import com.example.rest.api.UrlSpace;

//@formatter:off

public class ExampleApiUrls implements UrlSpace
{

	public final static String API 								 = "/api";
	public static final String LOG 							     = "/api/log";
	public static final String REGISTER 						     = "/api/register";
	
	public static final class Admin 
	{
		public static final String REPORTS                       = "/api/admin/reports";
		public static final String REPORTS_LOGINS                = "/api/admin/reports/login";
		public static final String REPORTS_USERS                 = "/api/admin/reports/users";
		public static final String REPORTS_USERS_DETAILS         = "/api/admin/reports/users_details";
		public static final String REPORTS_USERS_REGISTER_FAILED = "/api/admin/reports/users_register_failed";
		public static final String REPORTS_USERS_ACTIVITY        = "/api/admin/reports/users_activity_report_users";
		public static final String REPORTS_USERS_ACTIVITY_REPORT = "/api/admin/reports/users_activity_report/{userId}";
		public static final String REPORTS_LOGIN_TIMES           = "/api/admin/reports/login_times_report";
	}
	
	public static final class Photos 
	{
		public static final String PHOTOS                          = "/api/photos";
		public static final String PHOTOS_USER                     = "/api/photos/user";
		public static final String PHOTOS_USER_TAG                 = "/api/photos/user/{tag}";
		public static final String PHOTOS_USER_CROP                = "/api/photos/user/crop";
		public static final String PHOTOS_USER_EDIT_URLS           = "/api/photos/urls/user_profile";
		public static final String PHOTOS_COMPANY_LOGO             = "/api/photos/company/{id}/logo";
		public static final String PHOTOS_COMPANY_LOGO_TAG         = "/api/photos/company/{id}/logo/{tag}";
		public static final String PHOTOS_COMPANY_LOGO_CROP        = "/api/photos/company/{id}/logo/crop";
		public static final String PHOTOS_COMPANY_LOGO_EDIT_URLS   = "/api/photos/urls/company_logo/{company_id}";
		public static final String PHOTOS_COMPANY_BANNER           = "/api/photos/company/{id}/banner";
		public static final String PHOTOS_COMPANY_BANNER_TAG       = "/api/photos/company/{id}/banner/{tag}";
		public static final String PHOTOS_COMPANY_BANNER_CROP      = "/api/photos/company/{id}/banner/crop";
		public static final String PHOTOS_COMPANY_BANNER_EDIT_URLS = "/api/photos/urls/company_banner/{company_id}";
	}
	
}
//@formatter:off